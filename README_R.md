

# [2023生化危机4重制版FR概览](https://gitee.com/ksanl/RE4/blob/master/README.md)

> 注意该文档导出自 doc.re4gs.cn 作者保留所有权利 感谢支持盖世引擎
> 
> 您需要确保完整阅读本协议并知悉且 同意 的情况下
> 才可以使用 2023新传说:陨兔FR
> (以下统称本引擎或本驱动)
![](https://gitee.com/lioncky/RE4/raw/master/imgs/vagtMT.png)
## 【*扫盲】
> 没有喷子就没有这个板块 因为普通玩家容易被带节奏 故也叫下载须知 [点我查看使用协议](#使用协议)

### 1. 什么是EOE?
> 所谓EOE 全称为EngineOnEninge<br>
> 简要来说 就是基于游戏原生引擎重载的独立引擎<br>
> 在研发时间和开发难度上要远比修改器复杂的多 需要的专业性技能也要更强<br>
> 因此大部分玩家不具备客观的理论知识将EOE错解为修改器是较为片面的<br>
> 而EOE之所以实至名归 是因为能做更多单靠修改器无法做到的事情<br>
> 例如<br>
> 1. EOE跟随游戏自然启动 而修改器需要手动打开<br>
> 2. 彼此的寄生关系导致EOE每次调试都要等待漫长的游戏启动 而修改器可以实时调试既省时又省力<br>
> 3. EOE需要自我考虑内存的各种限制以及线程协程等各种问题 而修改器只需要修改汇编代码防止内存异常造成的闪退
> 4. 由于寄生关系EOE也可以大量直接使用游戏的原生函数 但也要承受函数内部不可预知的问题 需要额外逐层次分析
> 5. EOE几乎可以向游戏添加任何开发者想呈现的内容 而修改器作为跨进程外瓜没办法对游戏大规模精细化的修改
> 6. 自古阴阳同行 万事有利有弊 更多细节需要你成为更高级的工程师才会深刻了解 吃瓜MJJ请享受国产之光!
> 
### 2.如何下载EOE
> 你可以通过加入QQ群聊 669835782 获取 [Steam内部群](http://upt.re4gs.cn/) 
> 
> 或通过此链接获取 [2023新传说:陨兔](http://upt.re4gs.cn/Bin/fr.rar) 的最新版本 [安装方法](#如何安装EOE)

![](https://att.3dmgame.com/att/forum/202305/20/054604xbelounwlnywn5lf.jpg)
### 3. 为什么要开发EOE
```cpp
// 热爱
// 
// 大家跟我一样都很喜欢生化4 
// Leon李三光标标准准就是我的偶像
// 由于缺乏支持导致烂尾的项目数不胜数
// 因此希望大家有能力的支持一下 没能力的不推荐支持
// 一直以来作为作者看到了太多大家游玩的快乐和对国产的信心
// 而陨兔作为国内唯一EOE也将继续秉持初心 给配置不高的玩家也能带去快乐
// 最后希望大家发表言论前考虑下言行 别去伤害不该被伤害的人 也感谢大家的一路支持 谢谢！
```
### 4. 抄袭
```cpp
// 本来我想说 说就说吧 清者自清 后来我发现喷子有点强 因此就有了这个板块
//
// 之前有段时间有人说我把国外开源的玩意卖钱 我按照常规玩家的思维正常想了一下
// 好像的确在正常玩家眼里就是这样 跟修改器没有区别 但是啊 你们忽略了一个重要的问题
// 当你玩着新出的pj版 突然发现用不了正版插件的时候 惊奇的发现为啥所谓抄袭兔还是可以直接进游戏?
// 原因就是这玩意是我一个字一个字敲出来的 里面也没有所谓的抄袭代码 因此一切正常
// 另外虽然鼓励大家游玩正版 但是有的确情况特殊 作者在此承诺不会伤害特殊群体
// 希望大家别以小人之心去想所有人 自己不干净的自己知道就行了 
// 清者自清 非要搞得所有人都知道你啥货色干啥呢
```
### 5.优化 [参考3DM](https://bbs.3dmgame.com/thread-6406255-1-1.html)
*“——EOE几乎可以向游戏添加任何开发者想呈现的内容”*
```cpp
// 正如前面所说
// 测试默认三高情况下 7945hx+4070默认2K下只有72z 
// EOE解除了万恶资本家控制的游戏帧率限制将游戏稳稳的拉到120z
// 这意味着即使是 笔记本 也能毫无保留的展现所有性能
// 因此物质上支持我的高层玩家完全享受这个板块
// 属于单独开小灶了
```
> 另外 我们将官方的推荐的最低配置魔幻的保持了 
> 
> 这意味着即使如此外加各种独立的渲染图像 也能几乎保持原本的帧率
```
内存: 8 GB RAM
DirectX 版本: 12 Windows 版本: 10 或 11
处理器: AMD Ryzen 3 1200 / Intel Core i5-7500
显卡: AMD Radeon RX 560 with 4GB VRAM / NVIDIA GeForce GTX 1050 Ti with 4GB VRAM

推荐配置 内存: 16 GB RAM
DirectX 版本: 12 Windows 版本: 10 或 11
处理器: AMD Ryzen 5 3600 / Intel Core i7 8700
显卡: AMD Radeon RX 5700 / NVIDIA GeForce GTX 1070
```
## [使用协议](http://doc.re4gs.cn/)
> 如本文开头 您需要在知晓使用协议的情况下 合法履行个人权益
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTYwMjU3OTk3XX0=
-->