
## [返回上级](/README_UHD.md)
![](/imgs/vagtMT.png)
> 默认生化危机4终极高清版分为 v1.0.6 和 v1.1.0 双版本
> 1.0.6是纯英文版本 但是有明间的汉化包 支持最多旧mod
> 1.1.0是官方为中文特别提供的一个版本 但是汉化水平一般
> 两者的游戏路径一致 都包含 Bin32 和 BIO4
> ![](/imgs/2024-03-21/Mb4N8bKNAUGPPXbC.png)
### *Bin32*
> bio4.exe 默认启动器 不介绍
> steam_api.dll 正版 steam 加载器
### *BIO4*
> 存放游戏的数据文件
### *BIO4同级禁用选项*
```
v3.9.2.7 起支持以下选项
BAN_R100_DIE.txt      // 李老汉噶了触发 改版不兼容禁掉
BAN_R100_BRIGE.txt    // 走到桥上触发被警察叫回来 改版闪退禁掉
BAN_R100_HOUSE.txt    // 房子门口的闪抖 改版没处理导致闪退自行禁掉
```

<!--stackedit_data:
eyJoaXN0b3J5IjpbODkyNDQ3MzEzLC0xMTU0MDI4NjIwLDgxNz
cyNTY4NSwzNTk1ODk1NzIsLTE4Mjg1OTI5NjMsLTQ4ODYxNTYy
NSw1NjE4OTc2ODYsMjA0MzE3ODQ1Ml19
-->