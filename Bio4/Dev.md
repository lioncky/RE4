
>  
## 三好学生板块

> 生化危机4简单的机制说明 <br>
> 通过分析游戏源码得到的游戏特性
> 
### 0.游戏内部数据
>  [游戏内物品ID](/Bio4/LIST.md) 入坑基础知识
>  
### 1.血量机制
> 难度<=普通时 
> 动态伤害倍率= 2.0-难度/5
> 而普通MAX=5 此时伤害= 2-5/5=1.0 此时伤害正常
> 专家=6 此时伤害= 1-(6-5)*0.03=0.97 此时伤害略微下调了一点
```cpp
	if (dynamicDifficultyLevel_4F98 <= 5u)
		dydmg = 2.0 - (double)dynamicDifficultyLevel_4F98 * 0.2;
	else
		dydmg = 1.0 - (double)(dynamicDifficultyLevel_4F98 - 5) * 0.03;

	dydmg *= (double)dmg;
```
### 2.阵亡保护机制
```c
	// 如果难度为普通及以下
	// 或者血量大于300 即使伤害严重超标 也不至si
	if (! (pG->dynamicDifficultyLevel_4F98 >= 3u || curHp_4FB4 <= 300) )
		noDie = 1;

	// 当血量>300时 1/4概率 受到必死伤害也不会噶
	if (curHp_4FB4 > 300)
	{
		if ((j_Rnd((int)pSYS) & 3) == 0) // 0 4 8 C
			noDie = 1;
	}

	// noDie在一些特定情况下 也默认为1 例如打Krauser时
```

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTAwNjMwMjIxN119
-->