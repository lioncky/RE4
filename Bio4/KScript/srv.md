```lua
-- TM log linf lerr lwarn cls
-- qs(qsendhost) qsg qsf qsendgroup qn(nick)
local js = json
local su = 2424527764

--
-- cmd su 用于CQ和控制台两部分
--
local function cmd_su(msg)

	local arg = {} -- 分割文本 [+ 10001 积分 1]
		for word in msg:gmatch("%S+") do 
		    table.insert(arg, word)
		end -- "%S+" 表示匹配非空白字符
		print('su_arg', #arg, msg)
	
	local i = #arg
	if i == 1 then 
		if msg == 'help' then
			local h = '帮助:\n\n'
			h = h .. 'lrbt: 重启Lua\n'
			h = h .. 'tem: 测试邮件\n'
			h = h .. 'kcq: 重启CQ\nchm: 测试CQ发送\n'
			h = h .. '查询: [& 2424527764]\n'
			h = h .. '增减: [+/- 2424527764 积分 1]'
			return h
		elseif msg == 'lrbt' then LUA_REBOOT() return 'lua_rebooting' 
		elseif msg == 'qrbt' then CQ_REBOOT() return 'wait 1s->KILL_REBOT_CQ' 
		end
	else 
		local op = arg [1]
		if op == '+' and i == 4 then -- 添加 2424527764 积分 1
			return SQ_ADD(arg[2], arg[3], tonumber(arg[4])) and 'ok' or 'x'
		elseif op == '-' and i == 4 then -- 减少 2424527764 积分 1
			return SQ_SUB(arg[2], arg[3], tonumber(arg[4])) and 'ok' or 'x'
		elseif op == '&amp;' and i == 2 then -- 查询(&被转义) 2424527764 积分 1 
			return SQ_USERDATA(arg[2])
		end
	end
	
	return ''
end

function lua_recv_cmd(cmd) -- r:string
	if (cmd == nil) or (cmd == '') then return end
	
	if cmd == 'cls' then cls() 
	elseif cmd == 'x' then cls()
	else end
	print('cmd:', cmd_su(msg))
	return 
end

local function srv_msg(OP, params, sQ, sNick)
	print('OP', OP, sNick)
    if OP == 'CHAT' then

        local msg = string.format("%s: %s", sNick, params['MSG'])
        srv_send_all('OP=CHAT|MSG= ' .. msg)
        -- cq_send_xwc_msg(msg)

    elseif OP == 'KILLEM25' then
        srv_send_all('MSG= 玩家 ' .. sNick .. ' 已经暴走了!\n全服奖励一瓶养乐多|SPAWN=0-5-0-1')

        if sQ then 
            SQ_ADD(sQ,"杀敌",25)
            log(TM() .. sQ .. ' ' .. OP)
        end
        
    elseif OP == 'CRIT10' then
        srv_send_all('MSG= 玩家 ' .. sNick .. ' 已触发会心10次!\n登峰造极!活跃+10|SPAWN=0-2-0-1')
        if sQ then 
            SQ_ADD(sQ,"活跃",10)
            log(TM() .. sQ .. ' ' .. OP)
        end
        return ""
    elseif OP == 'DEADPL' then
        if sQ then 
            local msg = '玩家 ' .. sNick .. ' 噶了! ' 
			if params['ASHLEY'] == '1' then 
				msg = msg .. '\n倒在了艾什莉的裙下! ' 
			end
            if G_AAA == 0 then 
                G_AAA=1
                -- cq_send_laoyang_msg(msg)
                -- cq_send_xwc_msg(msg)
            else 
                G_AAA=0
                -- cq_send_xwc_msg(msg)
            end
            
			msg = 'MSGE= ' .. msg .. '活跃+3'
            print('msg', msg)
            srv_send_all(msg)

            SQ_ADD(sQ,"活跃",3)
            SQ_ADD(sQ,"阵亡",1)
            log(TM() .. sQ .. ' ' .. OP)
        end
        return ""

    -- print('OP',OP,'sQ',sQ)
    elseif OP == 'USERDATA' then
        if sQ then 
		    -- print('USERDATA-X', sQ, SQ_USERDATA(sQ))
            return 'NICK=' .. sNick ..'|USERDATA=' .. SQ_USERDATA(sQ)
        end
    end
    return ''
end


-- CQ AERA -- 
-- CQ AERA -- 
-- CQ AERA -- 

function lua_recv_srv(c, data) -- cid data(LCKY|OP=KILL) r:string

    local params = {} -- 解析键对
    for param in data:gmatch("[^|]+") do
        local key, value = param:match("([^=]+)=([^=]+)")
        if key then
            params[key] = value
        end
    end
    -- print(c, data)
    if not params['OP'] then return end
	
	local qq = c2q(c)
	local sQ = tostring(qq)
	local sNick= 'null'
    -- print('c2d',c, qq)
	if #sQ > 5  then sNick= qn(qq) else qq = 0 end
	
    linf(string.format("%s lua_recv_srv %s(%s) %s", TM(), sNick, sQ, data))
	local r = srv_msg(params['OP'], params, qq > 0 and sQ or null, sNick)
	print('r:',r)
	if r and #r > 1 then srv_send(c,r) return end
end

local function AT(qid) return string.format('[CQ:at,qq=%d]', qid) end

local function msg_f(msg)
	return ''
end

local function msg_g(gid, qq, msg)
	
	return ''
end

function lua_recv_cq(_) -- r:void
	local j = js.load_string(_)
	if (j == null) then return end
	
	local type = j['post_type']
	if not type then --
		if j['retcode'] and j['retcode'] == 0 then
			-- {"data":{"message_id":893839167},"echo":"","message":"","retcode":0,"status":"ok"}
			linf('LCQ-ECHO:' .. j['echo'])
		end
	elseif type == 'message' then
		-- local sss = j.sender.nickname
		-- print(sss)  ok
		local nick = j['sender']['nickname']
		local mid = j['message_id']
		local qq = j['user_id']
		local msg = j['message']
		type = j['message_type']
		
		if type == 'group' then
			-- linf('LCQ:' .. _)
			local gid = j['group_id']
			linf(string.format('G-%d %s(%d):%s', gid, nick, qq, msg))
			
			local r = msg_g(gid, qq, msg)
			if r and #r > 1 then qsg(gid,r) return end

		elseif type == 'private' then
			linf(string.format('F%s(%d):%s', nick, qq, msg))
			if qq == su then
				local r = cmd_su(msg)
				if r and #r > 1 then qsf(qq,r) return end
				r = msg_f(msg)
				if r and #r > 1 then qsf(qq,r) return end			
			end
		else
			-- no possible to here
		end
	
	elseif type == 'request' then
		type = j['request_type']
		if type == "group" then  -- group_id user_id flag
			type = j['sub_type']
			
		else
		
		end
	
	elseif type == 'notice' then
		type = j['sub_type']
		
		if not type then -- what?
			qs('type null ' .. _)
		elseif type == 'leave' then -- group_id user_id time
			
		elseif type == 'approve' then -- group_id user_id operator_id time
			
		elseif type == 'set' then -- group_id user_id 
		
		elseif type == 'unset' then -- group_id user_id 
		
		elseif type == 'ban' then -- duration group_id operator_id user_id
			print(string.format('%d 被 (%d) ban %dmin', j['user_id'], j['operator_id'], j['duration']))
		elseif type == 'lift_ban' then  -- duration=0 group_id user_id operator_id 
		
		elseif type == 'group_recall' then -- group_id user_id message_id
		
		elseif type == 'friend_recall' then  -- user_id message_id
			
		end
		
	elseif type == 'meta_event' then
	else 
		linf('LCQ:' .. _)
	end
	
end --  cq -> void

--DEADBEEF
```

[to](https://gitee.com/lioncky/RE4/raw/master/Bio4/KScript/srv.md)


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc1NTAzNzI1NiwxMDI1MzE0MTg5LDU0Nj
M0NDE4NCwtNjgyMDYzMDcsNzQ2NzMwODk4LC02NTUwNjIxNSwt
NjUxNDEwNzkwLDExMTUwNTQ1ODAsLTU4NjYyNjQsLTMxNDIwMj
AwOCwtMTk1MjU1NjcxNywtMzc3OTY5MzQ3LDEyMzAzNTY4MDgs
NzEyNzgzNTA4LC05NjEzNzU0MjgsMjEzOTQ5MTc3NywxNzYzMT
E0OTExLDIwNzY0OTQ2NjEsMzgyMjgxMTkxLC01NjM2Mzk2MDRd
fQ==
-->