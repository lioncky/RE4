

## [返回上级](/README_UHD.md)
![](/imgs/vagtMT.png)
记住本站 https://gitee.com/lioncky/RE4/blob/master/Bio4/GSL.md

## GSL倾龙独立改版文件说明
> 自v3.9.3.2以来 一部分改版作者越来越需要移植改版到UHD<br>
> 因此请耐心阅读本倾龙引擎改版教程<br>
> 默认你已拥有基础的改版素养
> 
---

### ESL刷怪拓展(v3931+)

> 倾龙路径 GSL\emleon0X.esl [X表示0-7]<br>
> 正常情况下游戏使用BIO4/ETC/emleon0X.esl来加载敌人<br>
> 
> 简单尝试：<br>
> 1.将邪恶上升的BIO4/ETC/xxx.esl文件复制到GSL目录下<br>
> 2.进入游戏可看到邪恶上升的怪已经如数呈现至当前游玩的游戏本体<br>
> 3.由于倾龙引擎的加载功能 所以你可以随意使用现有的敌人esl脚本 <br>
> 
> 目前仅支持 不死国度 模式(只要是存在于GSL文件内的敌人 开门刷档后将如数复活)

### AEV事件拓展(v3933+)
[GSL测试文件下载 会在游戏开局创建一个直达村庄的门](https://gitee.com/lioncky/RE4/releases/download/3.9.3.0/GSL.7z)
> 倾龙路径 GSL\AEV\rXXX.aev [100表示房间号]<br>
> 
> 介绍：<br>
> 1.aev单事件为156字节 目前支持添加最多16个aev事件<br>
> 2.拓展3C位置为倾龙位<br>
 ```c
3C 倾龙内部保留位 
3D 标志位
	SEC_QL_FLAG_ITM = 0x10, // 需要背包内存在某个物品
	SEC_QL_FLAG_ITMUSE = 0x20, /// 需要背包存在并使用
	SEC_QL_FLAG_EM = 0x40, // 需要击杀一定数量的敌人或所有
3E id位 // 04 表示手木仓子弹
3F ct位 // 物品或敌人个数
```

> 倾龙路径 GSL\AEV\rXXX.ban [文件大小 不限制]<br>
> 一般情况下 需要屏蔽系统默认的某个aev事件 请创建此文件<br>
> 16进制编辑器随便写入 36 将禁用36号事件<br>
> 写入 10 36 37 38 xx 每个字节的事件都会被禁用<br>
> 由此可轻松关闭游戏原生大门
> 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTE2MjY3MDg5NiwtOTU1Mjc0NjY2LDE4Nz
k2NDAzNzddfQ==
-->