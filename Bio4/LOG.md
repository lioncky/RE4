![](https://s1.ax1x.com/2022/08/15/vagtMT.png)
> 您需要遵守地方法律法规 并同意 [使用协议](#使用协议) 作者保留所有权利 本站链接 bio.re4gs.cn
> 最新日志请转至 [此处](https://gitee.com/lioncky/RE4/releases)
```c
// 之后的日志请转至 https://gitee.com/lioncky/RE4/releases

[2024/01/13 v3.9.0.5]
	1.加入龙行华夏特别版log  
	2.修复万人斩改版无法游玩的问题  
	3.调整wrapper只支持dinput8和X3DAudio  
	4.若Bin32目录下存在 3DM.txt 那么将无视steam  
	5.优化了一些操作细节体验
	
[2024/01/15 v3.9.0.3]
	1.调整 美杜莎爆破范围为安全范围
	2.优化 第一人称 切换时的模型消失
	3.优化 第一人称 无限子弹下的显示
	4.添加 物理门锁 支持于门的反面踹锁
	5.修正 克劳泽消失时的ESP显示空气问题
	6.修正 游玩部分改版时的切换武器闪退(@SCP)
	7.调整 微加速一键无敌UI块重新放至首页(@Lin)
	8.调整 陨虎掉落生态重载为默认不启用(@李大鸟)

	// EXTRA 第7地狱等基于Raz0r改版的内置学习(@虎哥不带菜)
	// 届时所有基于万人斩EOE改版引擎本次更新后将完美免去steam 

[2024/01/13 v3.9.0.2]
	1.添加 动态动作查看器 [教程视频] 
	2.添加 一个敌人的名称在调试板块
	3.修正 ESP部分生物失效(@李大鸟)
	4.调整 常量区范围最大值下调至1万
	5.调整 ESP范围扩大至5~150
	6.移除 广告式的自动弹窗更新
	7.移植 绝对秒杀(@LLLLL)
	8.添加 太极按钮手动更新
	

[2024/01/06 v3.9.0.1]
	1.修正 美杜莎之眼部分怪物无效的问题(@Wuiy)
	2.修正 极致加速状态惩罚者换弹失败的问题(@)
	3.修正 极致加速主角时踹门不开的问题(@)
	4.增强 美杜莎之眼支持 爆炸线 捕兽夹
	5.增强 美杜莎之眼支持 怀表支架块
	6.增强 美杜莎之眼支持 鲈鱼 乌鸦
	7.增强 美杜莎之眼支持 恶犬
	8.增强 美杜莎之眼支持 小蛇
	9.调整 自动拾取金钱为黄色
	

[2024/01/03 v3.9.0.0FIX]
	1.调整 巨人和村长二阶的追踪优先级(@天涯孤客)
	2.移植 来福无限火力 (@虎哥不带菜)
	3.移植 手枪の无限火力 (@虎哥不带菜)
	4.移植 喷子の无限火力 (@虎哥不带菜)
	5.移植 手雷の无限火力 (@虎哥不带菜)
	6.增强 美杜莎之眼 对城主的石化效果
	6.修正 美杜莎之眼 的闪退问题
	

[2024/01/02 v3.9.0.0]
	1.修正 指定体术为禁用(-1)时的异常问题
	2.修正 木屋防守战概率闪退(@天涯孤客)
	4.修正 自动拾取金砖时$显示异常的问题
	4.移植 v106 U3闪退 LFS优先加载(@LL)
	5.移植 V键为 随时 克劳萨鸡翅突进(@LL)
	6.调整 大型Boss追踪优先级(@天涯孤客)
	7.调整 Leon/Ada进入佣兵为默认服装
	8.添加 右下角标的选择按钮(@耀)
	9.优化 第一人称沉浸式体验效果
	

[2023/12/31 v3.8.9.9]
	1.修正 部分瞎子被识别成教徒的问题(@天涯孤客)
	2.修正 部分骑士头盔掉落后追踪失效(@天涯孤客)
	3.修正 第一人称状态下瞄准线的显示问题
	4.修正 子弹换弹时的闪退问题(@李大鸟)
	5.修正 re4tweaks的切换场景闪退问题
	6.修正 追踪矿车战的永恒式观光问题
	7.移植 T键随时上段体术 G随时下段体术
	8.移植 上段体术切换 支持佣兵四人组
	9.新增 动态+随体 支持追踪断头术


[2023/12/30 v3.8.9.8]
	1.修正 替换城堡营救艾什莉的红衣教主钥匙为手雷
	2.修正 商人金身导致的敌人金身问题(@天涯孤客)
	3.修正 VIP->美杜莎之眼橡皮怪闪退(@李大鸟)
	4.修正 数据->玩家血槽显示和锁定异常
	5.修正 子弹打空状态下的无限子弹异常
	6.优化 追踪铁瞎改为背后弱点(@虎哥)
	7.加入 ESP额外选项和显示范围
	8.加入 VIP->随意欺负艾什莉
	9.添加 玩家和艾什莉的无敌


[2023/12/29 v3.8.9.7]
	1.修正 瞄准光标一直显示的问题
	2.修正 骑士无法被追踪选中
	3.优化 界面UI显示顺序
	4.加入 ESP图标+范围
	5.加入 锁定玩家血量
	6.加入 锁定同伴血量
	7.加入 追踪任何对象
	8.加入 商人绝对金身
	9.重铸 追踪极速算法


[2023/12/27 v3.8.9.6]
	1.优化近距离下子弹追踪的算法失衡 
	2.优化第一人称UI默认状态下不显示 
	3.消除FPL激光炮对追踪的干扰问题 
	4.加入对子弹穿透数量的修改 
	5.加入按住LSHIFE停止追踪 
	6.加入 ESP 简约血条 
	7.加入追踪弱点位置 


[2023/12/26 v3.8.9.5]
	1.常量板块支持自动保存和读取 无需手动 
	2.修正Wesker杀手7换弹失帧 
	3.2024喜加一子弹追踪 
	4.微加速移动至首页 
	5.优化了反外挂逻辑 


[2023/12/25 v3.8.9.3]
	1.！！！加入万人斩破解终极方案 
	2.修正Ada惩罚者换弹失帧问题 
	3.修正游玩改版的闪退问题 
	4.设定显示万人斩破解状态 
	5.优化压力测试结果 

[2023/10/24 v3.8.9.2] 
	1.陨虎加入首个专业版功能  美杜莎之眼 
	2.修正了 (@李大鸟) 反应的地雷枪问题 
	3.如履平地 潜行射击移至Pro区域 
	4.添加了ESP敌人信息即时演算 
	5.添加实时的敌人个数 
	6.添加实时前后个数 
	7.对界面进行了美化 
	
[2023/10/22 v3.8.9.1]
1.升级威虎架构的远程刷怪
2.添加T10效率刷怪按钮
3.加入躯体强制回收消失
4.提升场景的模型对象
5.提升了整体的稳定
6.加入自动拾取提示

[2023/09/21 v3.8.9.0]
"1.修正村民黄大妈名称显示错误
"2.优化刀伤体术的修改逻辑
"3.修正士兵名称显示错误
"4.修正威斯克范围失效
"5.常量加入所有伤害
"6.修正部分汉化错误 // 修改物品个数	
	// 特别版 [2023/10/23 v3.8.8.8 SPECIAL]
	1.加入生化4第一人称街机模式

[2023/09/19 v3.8.8.0]
1.修正部分情况下打不开菜单的问题 
2.添加自绘组件不同武器的图标 
3.添加自绘瞄准光标 

[2023/09/18 v3.8.7.0]
1.加入街机UI组件 [功能等待9月21日更新] 
2.添加针对艾什莉的检查 
3.默认显示艾什莉距离 

[2023/09/17 v3.8.6.0]
1.常量块 威斯克推掌 所有角色范围 [高层独享] 
2.常量块 支持刀伤三人范围修改  [高层独享] 
3.默认解封克劳泽鸡翅碰撞范围  
4.默认移除敌人受伤间隔 [独家] 
5.加入 移速_极致 [配合间隔] 
6.常量块 支持刀伤体术修改 
7.添加物品ID(?)帮助一览 
8.潜行射击 [高层独享] 

[2023/09/16 v3.8.5.2]
1.上弹修正 支持地雷枪 惩罚者 马提尔达 [独家] 
2.加入12345切枪 C切到上一把枪 
3.F3F4飞天遁地 6^传送艾什莉 

[2023/09/16 v3.8.5.0]
1.添加上弹修正 加速时和任何情况下不会失败 
2.添加W键上面345分别飞天遁地传送艾什莉 
3.添加木柜三刀爆开 自动跳过新手教程 
4.添加对玩家速度进行直接修改 
5.添加对玩家无敌状态修改 

[2023/09/15 v3.8.3.0]
1.增强了自动拾取的稳定性 多次拾取时不冲突 
2.添加F5快捷重开 F7随时存档 F8发起交易 
3.添加白天光源 重绘主界面HUD 

[2023/09/13 v3.8.2.0]
1.添加物品自动拾取 支持背包满了自动打开 
2.添加双倍掉落和100%掉落 
3.添加伏羲八卦图特效 

[2023/09/12 v3.8.1.1]
1.添加物品高光和高光增强选项[新] 
2.修正合成草药无法叠加 
3.修正了一些汉化错误 

[2023/09/11 v3.8.0.1]
1.完美重写了游戏原生叠加 草药不参与但支持合成叠加 
2.继承了威虎的掉落物等一系列修正 
3.为日后拓展绘制提供了原生接口 
日后版本将会以本框架作拓展

TreeNode(2023/08)) {

[2023/08/23] v3.7.2
完成了所有re4tweaks的汉化
计划下个版本将陆续部署威虎

[2023/08/09] v3.6.1
汉化基本完成 预计下个版本完美
修正了部分玩家闪退问题

[2023/08/08] v3.2
汉化了Trainer的热键板块
ESP超视的左半边 修正了无法输入中文的问题

[2023/08/06] v3.1
汉化了Trainer的前两个板块

[2023/08/03] v3.0
v3.0终极版 汉化了除Trainer以外的所有

[2023/08/02] v3.D2
本D2 汉化了键盘 控制 和帧率

[2023/08/01] v3.D1
本D1 汉化了音频 鼠标

[2023/07/31] 
陨虎项目启动
}
``` 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjE0NjIzMjIwN119
-->